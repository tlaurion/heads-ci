FROM fedora

LABEL maintainer "Thierry Laurion <insurgo@riseup.net>"

#From https://github.com/osresearch/heads-wiki/blob/master/Building.md
RUN dnf install -y @development-tools gcc-c++ gcc-gnat zlib-devel perl-Digest-MD5 perl-Digest-SHA uuid-devel pcsc-tools qemu ncurses-devel lbzip2 libuuid-devel lzma elfutils-libelf-devel bc bzip2 bison flex git gnupg iasl m4 nasm patch python wget libusb-devel cmake pv bsdiff
#Fedora kernel building suggested packages 
RUN dnf install -y dnf-plugins-core
RUN dnf builddep kernel